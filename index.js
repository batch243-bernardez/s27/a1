// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}

// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
}



/*------------------Activity--------------------*/
// Users
{
    "id": 12,
    "firstName": "Adrian",
    "lastName": "Flores",
    "email": "aflores@mail.com",
    "password": "af0410",
    "isAdmin": false,
    "mobileNo": "09721839654"
}

// Orders
{
    "id": 24,
    "userId": 12,
    "productID" : 78,
    "transactionDate": "12-12-2023",
    "status": "paid",
    "total": 1900
}

// Products
{
    "id": 78,
    "name": "Scientific Calculator",
    "description": "Electronic calculator designed to perform complex mathematical operations",
    "price": 1499.99,
    "stocks": 5,
    "isActive": true,
}

